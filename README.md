# Digi Transport + TrafiSense Camera Gateway

## Setup

Digi Transport's ip address == 192.168.0.150 this was changed from the default value of 192.168.1.1 which conflicts with the office router.

The TrafiSense camera IPs are 192.168.0.100 & 192.168.0.101

The config.json file holds all the configuration values such as the cameras' IPs, the camera's cloud credentials, and endpoints.

The Digi Transport only accepts python code modules as zip files. Hence all dependencies are imported in `main.py` as zip files. In order to have changes in `/api` or `/packages` you'll need to run `build.sh`

A quick way to send over all the required files to the Digi Transport to run the program (`config.json`, `main.py`, `packages.zip`, and `api.zip`) use `ship.sh`.

## Troubleshooting

### I can't ssh to the Digi Transport!

The default key exchange algorithim fails for whatever reason. One solution for this issue is to pass a different key exchange algorithim as a flag for the `ssh` command.

try `ssh -o KexAlgorithms=diffie-hellman-group1-sha1 ats_admin@$DIGI_TRANSPORT_IP`. 

### The Digi Transport is returning websocket connection errors!

Both cameras need to be connected to the Digi Transport for the script to operate properly. At the moment it would be best to have the cameras connected via a switch (the small box networking device). The `main.py` script would be easily modified to only need to connect to one or to not fail when one connection is lost.  

#### But both cameras are connected!

Try rebooting the Digi Transport. Inside the shell: `ss570652>reboot`.

### The connections aren't HTTPS!

Unfortunately, due to the Digi Transport being stuck with python2.6, TLS > 1.0 does not seem possible, which is the current requirement for most secure connections. One can get connections over python2.6 leveraging the pyOpenSSL module. However, this package had direct bindings with the OpenSSL which, I believe, the Digi lacks.
