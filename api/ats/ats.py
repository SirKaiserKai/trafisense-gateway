"""
Code to interface with the All Traffic Solution data hub.
"""
import datetime
import json
import ssl
import threading
import websocket

class ATSDataHubClient(object):
    """
    Class which handles interfacing with the All Traffic Solutions data center.
    """

    def __init__(self, http_service, url, message_path="/v1/message", auth_path="/v1/authd"):
        self.url = str(url)
        self.message_url = self.url + message_path
        self.auth_url = self.url + auth_path

        self.http_service = http_service
        # self.websocket = websocket.WebSocketApp("ws://" + url)
        # self.thr = threading.Thread(target=ATSDataHubClient._run_websocket_connection,
        #                             args=[self],
        #                             kwargs={})

        self.jwt_header_value = None

    def auth(self, username, password):
        """
        Queries the TrafficCloud for auth credentials.
        """
        res = self.http_service.post(self.auth_url,
                                     headers={"Content-Type": "application/json"},
                                     data={"sn": username, "appKey": password})

        if res.status == 200:
            try:
                res_obj = json.loads(res.text)
                self.jwt_header_value = "JWT " + res_obj["token"]
            except KeyError:
                # TODO: Handle lack of key
                pass
            except ValueError:
                # TODO: Handle failure to load json.
                pass
        else:
            # TODO: Handle error(s)
            pass

    def connect(self):
        """
        Connects to the websocket server.
        """
        self.thr.start()

    def close(self):
        """
        Closes websocket connection.
        """
        try:
            self.websocket.close()
        except Exception as e:
            print(e)

    def _run_websocket_connection(self):
        """
        Runs the websocket connection in the background.
        """
        self.websocket.run_forever(sslopt={"cert_reqs": ssl.CERT_NONE})

    def send(self, message):
        """
        sends a message over the websocket connection.
        """
        self.websocket.send(json.dumps(message))

    def post_event(self, event, event_type):
        """
        Post event handles sending events to the TrafficCloud.
        """
        message = {
            "action": "put",
            "timestamp": datetime.datetime.now().isoformat(),
            "messageType": event_type,
            "message": event
        }
        return self.post(self.message_url, message)

    def post(self, url, message):
        """
        Posts a message via HTTP. Requires auth values.
        """
        return self.http_service.post(url, headers={"Authorization": self.jwt_header_value, 
                                                    "Content-Type": "application/json"},
                                      data=message)
