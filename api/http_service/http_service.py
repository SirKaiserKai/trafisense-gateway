"""
http_service is a service dedicated to handling HTTP requests.
"""
import urlparse
import urllib
import httplib
import json

import ssl

def post(url, headers, data):
    """
    makes a HTTPS connection and POSTs the data as an application/json type.
    """
    parsed_url = urlparse.urlparse(url)
    try:
        if parsed_url.scheme == "https":
            conn = httplib.HTTPSConnection(parsed_url.netloc)
        else:
            conn = httplib.HTTPConnection(parsed_url.netloc)

        conn.request("POST", parsed_url.path, json.dumps(data), headers)
        response = conn.getresponse()
        response.text = response.read()
        return response
    # except Exception as e:
    #     print(e)
    finally:
        conn.close()

def get(url, headers={}):
    """
    makes a HTTP connection and a GET request against the provided url.
    """
    parsed_url = urlparse.urlparse(url)
    try:
        if parsed_url.scheme == "https":
            conn = httplib.HTTPSConnection(parsed_url.netloc)
        else:
            conn = httplib.HTTPConnection(parsed_url.netloc)

        conn.request("GET", parsed_url.path, headers)
        response = conn.getresponse()
        response.text = response.read()
        return response
    finally:
        conn.close()
