"""
api handles any communication with the TrafiSense over the FLIR Public API.
"""
from .trafi_sense import TrafiSenseDevice
from .test_trafi_sense import TestTrafiSenseDevice
