"""
api handles any communication with the TrafiSense over the FLIR Public API.
"""
import websocket


class _APIWebSocket(websocket.WebSocketApp):

    def __init__(self, url, header=None,
                 on_open=None, on_message=None, on_error=None,
                 on_close=None, on_ping=None, on_pong=None,
                 on_cont_message=None,
                 keep_running=True, get_mask_key=None, cookie=None,
                 subprotocols=None,
                 on_data=None, trafi_sense_device=None):
        websocket.WebSocketApp.__init__(self,
                                     url,
                                    header,
                                     on_open,
                                     on_message,
                                     on_error,
                                     on_close,
                                     on_ping,
                                     on_pong,
                                     on_cont_message,
                                     keep_running,
                                     get_mask_key,
                                     cookie,
                                     subprotocols,
                                     on_data)

        self.trafi_sense_device = trafi_sense_device

    def handle_message(self, message):
        """
        Calls the trafi_sense_device's on_event method for the provided message.        
        """
        self.trafi_sense_device.on_message(message)

    def handle_close(self):
        """
        Prints the prints closed websocket.
        """
        print('Closed: ' + str(self))

    def handle_error(self, error):
        """
        Calls the trafi_sense_device's on_error method.
        """
        self.trafi_sense_device.on_error(error)
