import unittest
from .trafi_sense import TrafiSenseDevice

class TestHTTPService(object):
    def __init__(self): pass

    def get(url, stream=False):
        return None

    def post(url, data=None):
        return None

class TrafiSenseSubClass(TrafiSenseDevice):
    def __init__(self, camera_url, test_case):
        TrafiSenseDevice.__init__(self, camera_url, http_service=TestHTTPService)
        self.test_case = test_case

    # event handler methods which assert the correct event['type'] value maps 
    # to the correct method.
    def on_bad_presence_quality(self, event):
        self.test_case.assertEqual('BadPresenceQuality', event['type'])

    def on_bicycle_count(self, event):
        self.test_case.assertEqual('BicycleCount', event['type'])

    def on_bicycle_presence(self, event):
        self.test_case.assertEqual('BicyclePresence', event['type'])

    def on_day_night(self, event):
        self.test_case.assertEqual('DayNight', event['type'])

    def on_dilemma_zone(self, event):
        self.test_case.assertEqual('DilemmaZone', event['type'])

    def on_fallen_object(self, event):
        self.test_case.assertEqual('FallenObject', event['type'])

    def on_over_speed(self, event):
        self.test_case.assertEqual('OverSpeed', event['type'])

    def on_pedestrian(self, event):
        self.test_case.assertEqual('Pedestrian', event['type'])

    def on_presence(self, event):
        self.test_case.assertEqual('Presence', event['type'])

    def on_presence_level(self, event):
        self.test_case.assertEqual('PresenceLevel', event['type'])

    def on_ptz_preset(self, event):
        self.test_case.assertEqual('PtzPreset', event['type'])

    def on_queue(self, event):
        self.test_case.assertEqual('Queue', event['type'])

    def on_radar_presence(self, event):
        self.test_case.assertEqual('RadarPresence', event['type'])

    def on_smoke(self, event):
        self.test_case.assertEqual('Smoke', event['type'])

    def on_speed_alarm(self, event):
        self.test_case.assertEqual('SpeedAlarm', event['type'])

    def on_speed_drop(self, event):
        self.test_case.assertEqual('SpeedDrop', event['type'])

    def on_stop(self, event):
        self.test_case.assertEqual('Stop', event['type'])

    def on_under_speed(self, event):
        self.test_case.assertEqual('Underspeed', event['type'])

    def on_wrong_way_driver(self, event):
        self.test_case.assertEqual('WrongWayDriver', event['type'])

    def on_bad_video(self, event):
        self.test_case.assertEqual('BadVideo', event['type'])

    def on_configuration(self, event):
        self.test_case.assertEqual('Configuration', event['type'])

    def on_firmware_update(self, event):
        self.test_case.assertEqual('FirmwareUpdate', event['type'])

    def on_input(self, event):
        self.test_case.assertEqual('Input', event['type'])

    def on_no_video(self, event):
        self.test_case.assertEqual('NoVideo', event['type'])

    def on_temperature(self, event):
        self.test_case.assertEqual('Temperature', event['type'])

    def on_power_drop(self, event):
        self.test_case.assertEqual('PowerDrop', event['type'])

    def on_fuse_blown(self, event):
        self.test_case.assertEqual('FuseBlown', event['type'])

    def on_remote_device_connected(self, event):
        self.test_case.assertEqual('RemoteDeviceConnected', event['type'])


class TestTrafiSenseDevice(unittest.TestCase):

    def test_on_event_method_multiplexing(self):
        tsd = TrafiSenseSubClass('192.168.0.101', self)
        keys = tsd._options.keys()

        for k in keys:
            tsd.on_event({'type': k})


if __name__ == '__main__':
    unittest.main()