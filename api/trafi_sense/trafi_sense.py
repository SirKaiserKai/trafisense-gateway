# -*- coding: utf-8 -*-
"""
api handles any communication with the TrafiSense over the FLIR Public API.
"""
import json
import ssl
import threading
import time
from .api_websocket import _APIWebSocket

_SUBSCRIBE_MESSAGE = {
    "messageType": "Subscription",
    "subscription": {
        "type":"Event",
        "action":"Subscribe"
        }
}

class TrafiSenseDevice(object):
    """
    wraps the FLIR Public API.
    """

    def __init__(self, camera_url, http_service=None):
        # TODO: Do urlparse to check if url scheme is correct.

        self.camera_url = 'http://' + camera_url
        self.api_url = self.camera_url + '/api'
        self.device_url = self.api_url + '/device'

        # Connects over websocket connection
        self.websocket_connection = 'ws://' + camera_url + '/api/subscriptions'
        
        self.http_service = http_service

        self.websocket_app = _APIWebSocket(self.websocket_connection,
                                           on_error=_APIWebSocket.handle_error,
                                           on_close=_APIWebSocket.handle_close,
                                           on_message=_APIWebSocket.handle_message,
                                           trafi_sense_device=self)
        self.thr = threading.Thread(target=TrafiSenseDevice._run_websocket_connection,
                                    args=[self],
                                    kwargs={})
        
        # _options is used as make switch statement for the 'on_event' method to
        # act as multiplexer for the respective event method.
        self._options = {
            'BadPresenceQuality': self.on_bad_presence_quality,
            'BicycleCount': self.on_bicycle_count,
            'BicyclePresence': self.on_bicycle_presence,
            'DayNight': self.on_day_night,
            'DilemmaZone': self.on_dilemma_zone,
            'FallenObject': self.on_fallen_object,
            'OverSpeed': self.on_over_speed,
            'Pedestrian': self.on_pedestrian,
            'Presence': self.on_presence,
            'PresenceLevel': self.on_presence_level,
            'PtzPreset': self.on_ptz_preset,
            'Queue': self.on_queue,
            'RadarPresence': self.on_radar_presence,
            'Smoke': self.on_smoke,
            'SpeedAlarm': self.on_speed_alarm,
            'SpeedDrop': self.on_speed_drop,
            'Stop': self.on_stop,
            'Underspeed': self.on_under_speed,
            'WrongWayDriver': self.on_wrong_way_driver,
            'BadVideo': self.on_bad_video,
            'Configuration': self.on_configuration,
            'FirmwareUpdate': self.on_firmware_update,
            'Input': self.on_input,
            'NoVideo': self.on_no_video,
            'Temperature': self.on_temperature,
            'PowerDrop': self.on_power_drop,
            'FuseBlown': self.on_fuse_blown,
            'RemoteDeviceConnected': self.on_remote_device_connected
        }

    def connect(self):
        """
        handles connecting to the Flir TraffiSense camera.
        """
        self.thr.start()
        time.sleep(1)

    def close(self):
        """
        closes the websocket connection to the Flir TrafiSense device.
        """
        try:
            self.websocket_app.close()
        except:
            print('Error disconnecting')

    def protocol_version(self):
        """
        returns protocol version.
        """
        return self.http_service.get(self.api_url + '/version').json()

    def device_information(self):
        """
        returns device information.
        """
        return self.http_service.get(self.device_url + '/info').json()

    def device_time(self):
        """
        retrieves the current time from the device.
        """
        return self.http_service.get(self.device_url + '/time').json()

    def set_device_time(self, datetime):
        """
        sets the current time for the device.
        """
        self.http_service.post(self.device_url + '/time', data={'time': datetime.isoformat()})

    def device_uptime(self):
        """
        returns the uptime of the device.
        """
        self.http_service.get(self.device_url + '/uptime').json()

    def current_image(self):
        """
        retrieves the current image.
        """
        return self.http_service.get(self.api_url + '/image/current', stream=True)

    def image_for_event(self, event_number):
        """
        retrieves an image for a certain event by its event number (if available on device)
        """
        return self.http_service.get(self.api_url + '/image/event/' + event_number, stream=True)

    def video_for_event(self, event_number, pre_time, post_time):
        """
        retrieves the video for an event (if available on the device).
        """
        return self.http_service.get(self.api_url +
                                     '/video/event/' +
                                     event_number +
                                     '/?pre=' +
                                     pre_time +
                                     '&post=' +
                                     post_time)

    def supported_events(self):
        """
        Not every device has the same functionality and as such, cannot generate each event
        type supported by the protocol. This command allows to retrieve the list of event types
        that are supported by the device.
        """
        return self.http_service.get(self.api_url + '/events/supported').json()

    def open_events(self):
        """
        Returns a list of events which are currently open.
        """
        return self.http_service.get(self.api_url + '/events/open').json()

    def subscribe(self, event=None):
        """
        subscribes to an event stream on the camera.
        """
        self.send(_SUBSCRIBE_MESSAGE)

    ### Websocket methods ###

    def _run_websocket_connection(self):
        """
        Runs the websocket connnection in a seperate thread.
        """
        self.websocket_app.run_forever(sslopt={"cert_reqs": ssl.CERT_NONE})

    def on_message(self, message):
        """
        handles a message from the websocket connectiøn. Maps the event to the
        appropriate method handler.
        """
        try:
            json_msg = json.loads(message)
            message_type = json_msg['messageType']

            if message_type == 'Event':
                self.on_event(json_msg)
                return
            if message_type == 'ForceEvent':
                print(json_msg)
                return
            if message_type == 'Error':
                print(json_msg)
                return
        except:
            print(message)

    def on_event(self, event):
        """
        Responds to event.
        """
        try:
            event_type = event['type']
            event_method = self._options[event_type]
            event_method(event)
        except KeyError as excep:
            print(excep)

    def send(self, event=None):
        """
        Sends an event.
        """
        self.websocket_app.send(json.dumps(event))

    def on_error(self, error):
        """
        Prints the error message.
        """
        print('%s error: %s' % self.websocket_connection, error)

    def on_bad_presence_quality(self, event):
        """
        handler method for when quality of the video is too bad for detection.
        """
        pass

    def on_bicycle_count(self, event):
        """
        Stateless event triggered every time a bicycle leaves the zone.
        """
        pass

    def on_bicycle_presence(self, event):
        """
        Stateless event triggered every time a bicycle enters the zone.
        """
        pass

    def on_day_night(self, event):
        """
        Beginning of Night.
        """
        pass

    def on_dilemma_zone(self, event):
        """
        One or more vehicles are in the dilemma zone.
        """
        pass

    def on_fallen_object(self, event):
        """
        A fallen object was detected.
        """
        pass

    def on_over_speed(self, event):
        """
        One ore more vehicles is over speed.
        """
        pass

    def on_pedestrian(self, event):
        """
        A pedestrian is visible.
        """
        pass

    def on_presence(self, event):
        """
        An object is visible.
        """
        pass

    def on_presence_count_on_red(self, event):
        """
        Stateless event triggered every time a vehicle leaves
        the presence zone while the linked RYG channel is red.
        """
        pass

    def on_presence_level(self, event):
        """
        Provides the level of the presence.
        """
        pass

    def on_ptz_preset(self, event):
        """
        Indicates the current preset position for a PTZ camera.
        """
        pass

    def on_queue(self, event):
        """
        The vehicle threshold in the queue is reached.
        """
        pass

    def on_radar_presence(self, event):
        """
        Presence of one or more vehicles based on radar input.
        """
        pass

    def on_smoke(self, event):
        """
        Presence of Smoke is detected.
        """
        pass

    def on_speed_alarm(self, event):
        """
        Speed alarm was activated.
        """
        pass

    def on_speed_drop(self, event):
        """
        Speed drop occured.
        """
        pass

    def on_stop(self, event):
        """
        Vehicle stopped.
        """
        pass

    def on_under_speed(self, event):
        """
        Vehicle is under speed.
        """
        pass

    def on_wrong_way_driver(self, event):
        """
        handler method for wrong way driver.
        """
        pass

    ### Technical Event Handlers ###

    def on_bad_video(self, event):
        """
        Video contrast dropped below threshold.
        """
        pass

    def on_configuration(self, event):
        """
        Sent when a device has received a new configuration.
        """
        pass

    def on_firmware_update(self, event):
        """
        Active during firmware update.
        """
        pass

    def on_input(self, event):
        """
        Sent when a device input changes state.
        """
        pass

    def on_no_video(self, event):
        """
        Sent when a device loses video input (only for devices with external video input).
        """
        pass

    def on_temperature(self, event):
        """
        The device temperature is above the configured threshold.
        """
        pass

    def on_power_drop(self, event):
        """
        Sent when the power drops for a short period without reboot.
        """
        pass

    def on_fuse_blown(self, event):
        """
        Sent by interface boards when the fuse of one of the connections is blown.
        """
        pass

    def on_remote_device_connected(self, event):
        """
        Sent by BPL communication boards when connected with a remote device. Stays open until
        disconnected.
        """
        pass
