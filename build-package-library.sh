#!/bin/bash

# trap errors and print failure info
trap ': "*** BUILD FAILED ***" $BASH_SOURCE:$LINENO: error: "$BASH_COMMAND" returned $?' ERR

virtualenv --python=/usr/bin/python2.6 --no-site-packages packageenv

# Activate virtualenv
source packageenv/bin/activate

pip install -r requirements-py2.txt --target=./packages 

# wipe out all precompiled python and egg-info files and folders.
find packages -name "*.pyc" -delete
find packages -name "*.egg-info" | xargs rm -rf

cd packages
zip -9rv packages.zip .
mv packages.zip ..
cd ..
