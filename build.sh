#!/bin/bash

# trap errors and print failure info
trap ': "*** BUILD FAILED ***" $BASH_SOURCE:$LINENO: error: "$BASH_COMMAND" returned $?' ERR

# creates packages zip
cd packages
zip -9rv packages.zip .
mv packages.zip ..
cd ..

# Creates api zip
cd api
zip -9rv api.zip .
mv api.zip ..
cd ..