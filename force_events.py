"""
sends force_event messages for debugging purposes.
"""

import json
import sys
import time

# Imports the python pip packages.
if "packages.zip" not in sys.path:
    sys.path.insert(0, "packages.zip")

# Imports the local api module.
if "api.zip" not in sys.path:
    sys.path.insert(0, "api.zip")

from trafi_sense import TrafiSenseDevice

NUMBER_OF_FORCED_EVENTS = 5

FORCE_EVENTS = [
    {
        "messageType":"ForceEvent",
        "forceEvent":{
            "type":"BadVideo",
            "state":"Begin",
            "level":2,
            "zoneId":1
        }
    },
    {
        "messageType":"ForceEvent",
        "forceEvent":{
            "type":"WrongWayDriver",
            "position": {
                "x": "20",
                "y": "30",
            },
            "time":"2012-11-08T16:32:03.465-03:00",
            "zoneId":1
        }
    }
]

def main():
    """
    main function.
    """

    # Grab the config file.
    with open('config.json') as config_file:
        config = json.load(config_file)

    # Get all the config  values.
    camera_0_url = config['cameras'][0]['flir_camera_url']
    camera_1_url = config['cameras'][1]['flir_camera_url']

    sn0 = config['cameras'][0]['ats']['sn']
    appKey0 = config['cameras'][0]['ats']['appKey']

    sn1 = config['cameras'][0]['ats']['sn']
    appKey1 = config['cameras'][0]['ats']['appKey']

    all_traffic_sols_url = config['ats']['traffi_cloud_url']

    # Initiate the TrafiSense cameras objects.
    tsd0 = TrafiSenseDevice(camera_0_url)
    tsd1 = TrafiSenseDevice(camera_1_url)

    tsd0.connect()
    tsd1.connect()
    time.sleep(1) # Hacky way of waiting for the connections before subscribing.

    tsd0.subscribe()
    tsd1.subscribe()

    try:
        print("...Publishing forced events...")
        for _ in range(0, NUMBER_OF_FORCED_EVENTS):
            time.sleep(1)
            tsd0.send(FORCE_EVENTS[1])
            tsd1.send(FORCE_EVENTS[1])

        print("...finished publishing events...")
    finally:
        tsd0.close()
        tsd1.close()


if __name__ == "__main__":
    main()
