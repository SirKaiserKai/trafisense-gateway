"""
Main execution.
"""

import json
import sys
import time

# Imports the python pip packages.
if 'packages.zip' not in sys.path:
    sys.path.insert(0, 'packages.zip')

# Imports the local api module.
if 'api.zip' not in sys.path:
    sys.path.insert(0, 'api.zip')

from ats import ATSDataHubClient
from http_service import http_service
from trafi_sense import TrafiSenseDevice

LOG_EVENTS = False

class TrafiSenseCamera(TrafiSenseDevice):
    """
    Subclass of TrafiSenseDevice.
    """

    def __init__(self, camera_url, ats_url, http_service):
        TrafiSenseDevice.__init__(self, camera_url, http_service)
        self.ats_client = ATSDataHubClient(http_service, ats_url)

    def close(self):
        """
        closes the websocket connections.
        """
        TrafiSenseDevice.close(self)

    def on_event(self, event):
        """
        prints the event.
        """
        TrafiSenseDevice.on_event(self, event)

    def on_wrong_way_driver(self, event):
        """
        handler method for wrong way driver.
        """
        resp = self.ats_client.post_event(event, "alarm")
        global LOG_EVENTS
        if LOG_EVENTS:
            print(resp.text)


def main():
    """
    main function.
    """
    print('...reading config file...')

    # Grab the config file.
    with open('config.json') as config_file:
        config = json.load(config_file)

    # Get all the config  values.
    camera_0_url = str(config['cameras'][0]['flir_camera_url'])
    camera_1_url = str(config['cameras'][1]['flir_camera_url'])

    sn0 = config['cameras'][0]['ats']['sn']
    appKey0 = config['cameras'][0]['ats']['appKey']

    sn1 = config['cameras'][0]['ats']['sn']
    appKey1 = config['cameras'][0]['ats']['appKey']

    all_traffic_sols_url = config['ats']['traffi_cloud_url']

    print('...finished getting config variables...')

    global LOG_EVENTS
    LOG_EVENTS = config['debug']['log']

    if LOG_EVENTS:
        print("...logging responses...")

    # Initiate the TrafiSense cameras objects.
    tsd0 = TrafiSenseCamera(camera_0_url, all_traffic_sols_url, http_service)
    tsd1 = TrafiSenseCamera(camera_1_url, all_traffic_sols_url, http_service)
    tsd0.connect()
    tsd1.connect()

    tsd0.ats_client.auth(sn0, appKey0)
    tsd1.ats_client.auth(sn1, appKey1)

    tsd0.subscribe()
    tsd1.subscribe()

    try:
        while True: 
            # Never ending loop until connections are closed or exception occurs.
            pass
    finally:
        tsd0.close()
        tsd1.close()


if __name__ == "__main__":
    while True:
        print('\n...running main method...')
        try:
            main()
            break
        finally:
            print('...ending...')

