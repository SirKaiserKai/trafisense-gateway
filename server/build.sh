#!/bin/bash

# trap errors and print failure info
trap ': "*** BUILD FAILED ***" $BASH_SOURCE:$LINENO: error: "$BASH_COMMAND" returned $?' ERR

# print commmands executed, exit on errors
set -eExuo pipefail

# print version of bash we're using
echo "BASH VERSION: $BASH_VERSION"

# set the expected pip version
PIP_VERSION=9.0.1

rm -rf ./build/
mkdir -p build

# create virtual enviornment
virtualenv -p `which python2` ./build/.env

cd ./build/.env
# turns off 'u' flag.
set +u
source ./bin/activate # activates the virtual env.
# turns on 'u' flag
set -u

# upgrade pip
pip3 install --upgrade pip==${PIP_VERSION}

# emit python and pip versions
python3 --version
pip3 --version

# install python dependencies 
pip3 install -r ../../requirements.txt --no-cache-dir

cd ../..

# basic syntax checking
python3 -m compileall *.py

# more involved pylint checking
# TODO reenable this
# pylint *py

# success
: "*** BUILD SUCCESSFUL ***"


