from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base

# DATABASE = 'sqlite:////server/db.db'


# db_session = scoped_session(sessionmaker(autocommit=False,
#                                          autoflush=False,
#                                          bind=engine))

Base = declarative_base()

class Database():
    def __init__(self, connection_url):
        engine = create_engine(connection_url, convert_unicode=True)
        self.db_session = scoped_session(sessionmaker(autocommit=False, autoflush=False, bind=engine))

        Base.query = self.db_session.query_property()
        import models
        Base.metadata.create_all(bind=engine)
