"""
contains the server's object models.
"""
from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

from db import db

class TrafiSenseDevice(db.Base):
    __tablename__ = 'device'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    ip = Column(String)

    def __init__(self, name=None, ip=None):
        self.name = name
        self.ip = ip
    
    def __repr__(self):
        return '<Camera %r>' % (self.name)

class WebHook(db.Base):
    __tablename__ = 'webhook'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    device_id = Column(Integer, ForeignKey('device.id'))
    url = Column(String)
    device = relationship(TrafiSenseDevice)

    def __init__(self, name=None, device_id=None, url=None, device=None):
        self.name = name
        self.device_id = device_id
        self.url = url
        self.device = device
    
    def __repr__(self):
        return '<Webhook %r>' % (self.name)