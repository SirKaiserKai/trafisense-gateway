"""
resources contains the logic for different routes.
"""
import sqlite3

from models import models
# from server.models import models
from db import db

from flask_restful import Resource

class Device(Resource):
    def get(self, device_id=None):
        if device_id:
            return models.TrafiSenseDevice.query.filter(models.TrafiSenseDevice.id == device_id).first()
        else:
            return models.TrafiSenseDevice.query.all()

    def post(self, name=None, ip=None):
        device = models.TrafiSenseDevice(name=name,
            ip=ip)
        db.db_session.add(device)
        db.db_session.commit()

        