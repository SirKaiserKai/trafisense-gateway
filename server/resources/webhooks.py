"""
resources contains the logic for different routes.
"""
from models import models
from db import db

from flask_restful import Resource

class WebHooks(Resource):
    def get(self, web_hook_id=None):
        if web_hook_id:
            return models.WebHook.query.filter(models.WebHook.id == web_hook_id).first()
        else:
            return models.WebHook.query.all()

    def post(self, name=None, device_id=None, url=None):
        device = models.TrafiSenseDevice.query.filter(models.TrafiSenseDevice.id == device_id)
        wh = models.WebHook(name=name,
            device_id=device_id,
            url=url,
            device=device)
        db.db_session.add(wh)
        db.db_session.commit()
        