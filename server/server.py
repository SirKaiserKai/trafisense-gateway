"""
server holds the functionality for the server that acts as a bridge between the
TrafiSense devices and any external service.
"""
from flask import Flask
from flask_restful import Api

from db import db
from resources import webhooks
from resources import devices
from resources import hello

APP = Flask(__name__)
API = Api(APP)
DATABASE_URL = 'sqlite:///db.db'

database = db.Database(DATABASE_URL)

def setup():
    """
    run before application in order to setup app & database.
    """
    pass

@APP.teardown_appcontext
def shutdown_session(exception=None):
    """
    shutdown_session exits a database session connection.
    """
    database.db_session.remove()
    if exception:
        print(exception)

API.add_resource(devices.Device, '/devices')
API.add_resource(webhooks.WebHooks, '/webhooks')
API.add_resource(hello.Hello, '/hello')

if __name__ == '__main__':
    setup()
    APP.run(debug=True)
